# Anytone AT-D8x8UV CodePlug Generator for Switzerland

## Purpose 

This collection of scripts will generate Channels.csv, Contacts.csv and 
Zone.csv from the Brandmeister.Network API for all Swiss repeaters. The 
CSV Format is suitable for Anytone 8x8 DMR Radios.

Source Code at: https://gitlab.com/wunderlins/anytone

(c) 2019, Simon Wunderlin, GPL v2

## Development Environment Setup

1. clone the repository: `git clone https://gitlab.com/wunderlins/anytone.git`
1. You need python 3.7 or later , install it with your package manager or get it here: https://www.python.org/downloads/
1. install the required python libraries for your OS, from the project folder: `.\tools\install_py_modules.bat` or `./tools/isntall_py_modules.sh`
1. that's it, you should be able to run `./contacts_json2csv.py` or `./repeater_json2csv.py` from the project folder (internet connection required)

## Generating Windows Executables

You need to install the `pyinstaller` package to make this happen:
`pip install pyinstaller`. 

Then just run (from the Project Directory) the following script:
`./tools/makexe.bat`. If everything goes according to plan, the generated 
executables should be in the `./dist` foleder. If not sacrifice a goat on 
the next full-moon night to the killer rabbit and try again (or use the 
wholy grenade as adviced).



