#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os, sys, json, csv
from urllib.request import urlopen, HTTPError # Python 3

"""
Input Format (json):
        {
            "fname": "Mathieu",
            "name": "Mathieu",
            "country": "Canada",
            "callsign": "VA3ECM",
            "city": "Ottawa-Hull",
            "surname": "Goulet",
            "radio_id": 1023002,
            "id": 1023002,
            "remarks": "CCS7",
            "state": "Quebec"
        },


Output Format (Anytone Contacts CSV):
"No.","Radio ID","Callsign","Name","City","State","Country","Remarks","Call Type","Call Alert"
"1","1023001","VE3THW","Wayne Edward","Toronto","Ontario","Canada","","Private Call","None"

"""

## Download contacts list
json_str = None
url="https://www.radioid.net/static/users.json" # radioid has no location nor tg information
#url="https://api.brandmeister.network/v1.0/repeater/?action=LIST" # brandmeister seems to be dank, all the interesting stufff there!
try:
	response = urlopen(url)
	json_str = response.read()

except HTTPError as e:
	print("Error encountered: %s for %s", str(e), url, file=sys.stderr)
	sys.exit(2)
except Exception as e:
	print("Post request raised exception: %s", e, file=sys.stderr)
	sys.exit(3)

json_data = json.loads(json_str)

# prepare output
f = open('Contacts.csv', 'w', newline='')
with f:
	writer = csv.writer(f, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)
	writer.writerow(["No.","Radio ID","Callsign","Name","City","State","Country","Remarks","Call Type","Call Alert"])

	for i, rec in enumerate(json_data["users"]):
		writer.writerow([
			i+1,
			rec["radio_id"],
			rec["callsign"],
			str(rec["fname"]) + " " + str(rec["surname"]),
			rec["city"],
			rec["state"],
			rec["country"],
			rec["remarks"],
			"Priavte Call",
			"None"
		])