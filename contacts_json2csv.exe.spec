# -*- mode: python -*-

block_cipher = None


a = Analysis(['contacts_json2csv.py'],
             pathex=['C:/msys64/usr/lib/python3.7/', 'C:/Users/wus/Documents/Anytone/downloader'],
             binaries=[('.\\_csv.dll', '.'), ('.\\_struct.dll', '.')],
             datas=[],
             hiddenimports=['/usr/lib/python3.7/csv.py', '/usr/lib/python3.7/struct.py'],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          [],
          exclude_binaries=True,
          name='contacts_json2csv',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          console=False , icon='app.ico' )
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               name='contacts_json2csv')
