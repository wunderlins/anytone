# -*- mode: python -*-

block_cipher = None

added_files = [
         ( './cache', 'cache' ),
         ( './share', 'share' ),
         ( './DMR Regionen.kml', '.' )
         ]
				 
a = Analysis(['../repeater_json2csv.py'],
             pathex=['../lib', '../'],
             binaries=[],
             datas=added_files,
             hiddenimports=['geopy'],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          [],
          name='repeater',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          runtime_tmpdir=None,
          console=True , icon='../share/app.ico' )
