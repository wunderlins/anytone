#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os, sys, re
sys.path.append(os.path.abspath('./lib'))
from lib.bmwsapi import *

## CONFIGURATION
basedir = None
cachedir = "cache/"
callsign = "HB9HBC"
outfile_channel = "Channel.csv"
outfile_zone = "Zone.csv"
exclude_hardware_re = "^MMDVM.*"

repeater_cache_time_sec = 2*60*60
repeaterlist_cache_time_sec = 2*60*60

kml_file = "DMR Regionen.kml"
out_dir = os.path.dirname(__file__) # executable path
basedir = os.path.dirname(__file__) # bundle directory

if getattr( sys, 'frozen', False ) :
	# running in a bundle
	basedir = sys._MEIPASS
	cachedir = os.getenv("TEMP") + "/repeater_cache"
	if os.path.isdir(cachedir + "/repeatertg") == False:
		os.makedirs(cachedir + "/repeatertg", exist_ok=True)
	out_dir = os.path.dirname(sys.executable)

km_file = out_dir + "/" + kml_file

print("out: ", out_dir, "base: ", basedir)

if not os.path.isfile(km_file):
	source = basedir + "/share/" + kml_file
	dest = km_file
	with open(source, 'r') as src, open(dest, 'w') as dst: 
		dst.write(src.read())
	
regions = kml.extract_coords(km_file)

sys.stdout.write("Fetching repeater list ...\n")
rlist, parsing_errors = get_repeater_list(cachedir, repeater_cache_time_sec)
sys.stdout.write("	Done. {:d} parsing errors in {} records.\n".format(parsing_errors, len(rlist)))
#pprint.pprint(rlist)

# find all repeaters within a radius of 200km of the center of switzerland
# 46.8182° N, 8.2275° E
xoord_range = geobounds(46.8182, 8.2275, 150)
rlist_filtered = repeater_by_radius(rlist, 46.8182, 8.2275, 200)

# pprint.pprint(rlist_filtered)

# start csv logger
f = open(outfile_channel, 'w', newline='')
writer = csv.writer(f, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)

"""
"No.","Channel Name","Receive Frequency","Transmit Frequency","Channel Type","Transmit Power","Band Width","CTCSS/DCS Decode","CTCSS/DCS Encode","Contact","Contact Call Type","Radio ID","Busy Lock/TX Permit","Squelch Mode","Optional Signal","DTMF ID","2Tone ID","5Tone ID","PTT ID","Color Code","Slot","Scan List","Receive Group List","TX Prohibit","Reverse","Simplex TDMA","TDMA Adaptive","Encryption Type","Digital Encryption","Call Confirmation","Talk Around","Work Alone","Custom CTCSS","2TONE Decode","Ranging","Through Mode","Digi APRS RX","Analog APRS PTT Mode","Digital APRS PTT Mode","APRS Report Type","Digital APRS Report Channel","Correct Frequency[Hz]","SMS Confirmation","Exclude channel from roaming"
"1","OS ALL","430.01250","430.01250","D-Digital","Low","12.5K","Off","Off","Echo","Private Call","HB9HBC","Always","Carrier","Off","1","1","1","Off","1","1","None","None","Off","Off","Off","Off","Normal Encryption","Off","Off","Off","Off","251.1","1","Off","Off","Off","Off","Off","Off","1","0","Off","off"
"71","HB9BA 2284-2","438.22500","400.00000","D-Digital","High","12.5K","Off","Off","2284 BS/BL","Group Call","HB9HBC","Always","Carrier","Off","1","1","1","Off","1","2","None","None","Off","Off","Off","Off","Normal Encryption","Off","Off","Off","Off","251.1","1","Off","Off","Off","Off","Off","Off","1","0","Off","off"
"27","D 433.450","433.45000","433.45000","D-Digital","High","12.5K","Off","Off","Simplex","Group Call","HB9HBC","Always","Carrier","Off","1","1","1","Off","1","2","None","RXALL1-BM","Off","Off","Off","Off","Normal Encryption","Off","Off","Off","Off","251.1","1","Off","Off","Off","Off","Off","Off","1","0","Off","off"
"""
writer.writerow(["No.","Channel Name","Receive Frequency","Transmit Frequency","Channel Type","Transmit Power","Band Width","CTCSS/DCS Decode","CTCSS/DCS Encode","Contact","Contact Call Type","Radio ID","Busy Lock/TX Permit","Squelch Mode","Optional Signal","DTMF ID","2Tone ID","5Tone ID","PTT ID","Color Code","Slot","Scan List","Receive Group List","TX Prohibit","Reverse","Simplex TDMA","TDMA Adaptive","Encryption Type","Digital Encryption","Call Confirmation","Talk Around","Work Alone","Custom CTCSS","2TONE Decode","Ranging","Through Mode","Digi APRS RX","Analog APRS PTT Mode","Digital APRS PTT Mode","APRS Report Type","Digital APRS Report Channel","Correct Frequency[Hz]","SMS Confirmation","Exclude channel from roaming"])

# get detail information for all routers
rlist_details = []
no = 1

zones = []
for rname, rpoly in regions:
	zones.append(
		[rname, []]
	)
	
p = re.compile(exclude_hardware_re)
for r in rlist_filtered:
	
	# skip MMDV hardware
	if p.match(r["hardware"]):
		continue
	
	#if r["hardware"][0:5] == :
	#	continue
	
	# skip repeaters without shift
	if r["rx"] == r["tx"]: # skip simplex systems
		continue

	# debug code
	#if r["callsign"][0:3] != "HB9": # drill down swiss reptr only, for testing purposes
	#	continue
	
	# fetch talk group info for repeater
	# https://api.brandmeister.network/v1.0/repeater/?action=get&q=204342
	details = fetch("repeater/?action=profile&q="+str(r["repeaterid"]),
									cachefile=cachedir+'/repeatertg/'+str(r["repeaterid"])+'.json', cachemaxage=repeater_cache_time_sec)
	
	# find zone
	zone = []
	zonenum = 0
	for rname, rpoly in regions:
		inside = point_inside_polygon(r["lng"], r["lat"], rpoly)
		if inside: 
			zone.append(zonenum)
		zonenum += 1
	
	if zone == []:
		continue
	
	for tg in details["staticSubscriptions"]:
		print("		", r["callsign"], r["repeaterid"], tg["talkgroup"], tg["slot"])
		
		"""
		writer.writerow([r["callsign"], r["repeaterid"], r["colorcode"], 
			r["lat"], r["lng"], r["agl"], r["rx"], r["tx"], tg["talkgroup"], 
			tg["slot"], r["last_updated"].strftime("%Y-%m-%d %H:%M:%S"), 
			r["city"], r["firmware"], r["hardware"], r["status"], r["website"], ','.join([str(i) for i in zone])])
		"""
		name = r["callsign"] + " " + str(tg["talkgroup"])
		rec = [str(no),name,
		       r["rx"],r["tx"],"D-Digital",
		       "High","12.5K","Off","Off",
		       str(tg["talkgroup"]),"Group Call",callsign,"Always","Carrier",
					 "Off","1","1","1","Off",str(r["colorcode"]),str(tg["slot"]),"None","None","Off","Off",
					 "Off","Off","Normal Encryption","Off","Off","Off",
					 "Off","251.1","1","Off","Off","Off",
					 "Off","Off","Off","1","0","Off","off"]
		writer.writerow(rec)
		no += 1
		
		for i in zone:
			zones[i-1][1].append(name)
f.close()

# generate the zone file
# "No.","Zone Name","Zone Channel Member","A Channel","B Channel"
f = open(outfile_zone, 'w', newline='')
writer = csv.writer(f, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)
writer.writerow(["No.","Zone Name","Zone Channel Member","A Channel","B Channel"])

i=1
for z in zones:
	writer.writerow([i, z[0], "|".join(z[1]), z[1][0], z[1][1]])
	#print(z[0], ",", "|".join(z[1]))
	i += 1

# Generate TalkGroups.csv
