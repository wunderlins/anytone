#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
This is the communication library for the Brandmeister.Network WebService API
(hence the name)
"""
import os, sys, math, json, csv
sys.path.append(os.path.abspath('./lib'))
from urllib.request import urlopen, HTTPError # Python 3
import platform
import time, datetime
import geopy
import geopy.distance
import kml

base_url = "https://api.brandmeister.network/v1.0/"

def geobounds(lat, lon, km):
	diagonal = math.sqrt(km**2*2)
	d = geopy.distance.distance(kilometers = diagonal)
	
	lat1, lon1, alt2 = d.destination(point=(lat, lon), bearing=315)
	lat2, lon2, alt2 = d.destination(point=(lat, lon), bearing=135)
	
	return [
		(lat2, lon1),
		(lat1, lon2)
	]


def point_inside_polygon(x, y, poly, include_edges=True):
	'''
	https://stackoverflow.com/questions/39660851/deciding-if-a-point-is-inside-a-polygon-python
	
	Test if point (x,y) is inside polygon poly.

	poly is N-vertices polygon defined as 
	[(x1,y1),...,(xN,yN)] or [(x1,y1),...,(xN,yN),(x1,y1)]
	(function works fine in both cases)

	Geometrical idea: point is inside polygon if horisontal beam
	to the right from point crosses polygon even number of times. 
	Works fine for non-convex polygons.
	'''
	n = len(poly)
	inside = False

	p1x, p1y = poly[0]
	for i in range(1, n + 1):
		p2x, p2y = poly[i % n]
		if p1y == p2y:
			if y == p1y:
				if min(p1x, p2x) <= x <= max(p1x, p2x):
					# point is on horisontal edge
					inside = include_edges
					break
				elif x < min(p1x, p2x):  # point is to the left from current edge
					inside = not inside
		else:  # p1y!= p2y
			if min(p1y, p2y) <= y <= max(p1y, p2y):
				xinters = (y - p1y) * (p2x - p1x) / float(p2y - p1y) + p1x

				if x == xinters:  # point is right on the edge
					inside = include_edges
					break

				if x < xinters:  # point is to the left from current edge
					inside = not inside

		p1x, p1y = p2x, p2y

	return inside

def file_modification_tst(path_to_file):
	"""
	Try to get the date that a file was modified ins sconds sind the epoch (01-01-1970)
	"""
	if platform.system() == 'Windows':
		return os.path.getmtime(path_to_file)
	else:
		stat = os.stat(path_to_file)
		return stat.st_mtime


def fetch(uri, callback=None, cachefile=None, cachemaxage=None):
	"""low level function to get data via http, with solid error handling 
	
	`uri` String is the part after the base service url `base_url`, ie.
	'repeater/?action=LIST'
	
	`callback` function optional: is a callback function which gets every element 
	in the json list  as first parameter. the cb function can then transform 
	data and has to return the transformed object. Example interface
		def mycb(element): return element;
	
	`cachefile` String optional: if set together with `cachemaxage` the raw json 
	result will be cached on disk to lessen the load on the service and speed up 
	the application.
	
	`cachemaxage` Integer optional: this is the maximum age a cache file in 
	seconds. If the cache file is older then data will be retrieved and cached 
	again prior to processing it's content. None/0 means do not cache.
	
	"""
	
	url = base_url + uri
	obj_buffer = None
	json_str = ""
	
	# check if we have a cache file and if it is recent enough to be used
	fp = None
	valid_age = False
	try:
		fp = open(cachefile, "rb") # will raise an exception if the file does not exist
		now = time.time() # seconds since unix epoch
		mtime = file_modification_tst(cachefile)
		"""
		print(now)
		print(mtime)
		print(cachemaxage)
		print((mtime + cachemaxage))
		"""
		if (mtime + cachemaxage) > now:
			valid_age = True
		else:
			fp.close()
	except Exception as e:
		#print("Error ", e)
		pass
	
	#return "[]";
	
	# use the cached file
	if valid_age:
		print("\tUsing cached file {}".format(cachefile))
		json_str = fp.read()
		fp.close
	
	# fetch anew from the intertubes
	else: 
	
		print("\tFetching from http {}".format(uri))
		try:
			response = urlopen(url)
			json_str = response.read()

		except HTTPError as e:
			print("Error encountered: %s for %s", str(e), url, file=sys.stderr)
			sys.exit(2)
		except Exception as e:
			print("Post request raised exception: %s", e, file=sys.stderr)
			sys.exit(3)
		
		if cachefile:
			f = open(cachefile, "wb")
			f.write(json_str)
			f.close()
	
	obj_buffer = json.loads(json_str)
	
	# if there is a cllback function, run it on the json object
	if callback:
		obj_buffer = callback(obj_buffer)
	
	return obj_buffer

def get_repeater_list(cachedir, repeater_cache_time_sec):
	"""
	This method fetches a list of repeaters from Brandmeister.Network
	
	The json file provides all data as string elements, we try to convert them to 
	sane datatypes like lat/lon as float, repeaterid as int, etc.
	
	Example record:
	{'agl': '1',
	'callsign': 'AD5DO',
	'city': 'Caledonia, EM53TP',
	'colorcode': '1',
	'description': None,
	'firmware': '20181107_Pi-Star',
	'gain': None,
	'hardware': 'MMDVM_MMDVM_HS_Hat',
	'lastKnownMaster': '3102',
	'last_updated': '2019-07-01 00:52:39',
	'lat': '33.663071',
	'lng': '-88.334541',
	'pep': None,
	'priorityDescription': None,
	'repeaterid': '314389801',
	'rx': '446.2000',
	'status': '4',
	'tx': '446.2000',
	'website': 'http://www.qrz.com/db/AD5DO'},
	"""
	
	def cb(obj_buffer):
		parsing_errors = 0
		
		def toint(e, name, mandatory=True):
			try:
				e[name] = int(e[name])
			except:
				if mandatory:
					e["parsing_error"] = True
					print("Error converting {} for {}: {}".format(name, e["callsign"], e[name]), file=sys.stderr)
				e[name] = None
			
			return e
		
		def tofloat(e, name, mandatory=True):
			global parsing_error, parsing_errors
			try:
				e[name] = float(e[name])
			except:
				if mandatory:
					e["parsing_error"] = True
					print("Error converting {} for {}: {}".format(name, e["callsign"], e[name]), file=sys.stderr)
				e[name] = None
			
			return e
		
		out = []
		for e in obj_buffer:
			e["parsing_error"] = False
			e["last_updated"] = datetime.datetime.strptime(e["last_updated"], "%Y-%m-%d %H:%M:%S").date()
			
			# lat/long as float, optional
			e = tofloat(e, "lat", False)
			e = tofloat(e, "lng", False)
			if e["lat"] == None or e["lng"] == None:
				e["lat"] = None
				e["lng"] = None
			
			# agl as int, optional
			e = toint(e, "agl", False)
			# colorcode as int, mandatory
			e = toint(e, "colorcode", True)
			# lastKnownMaster as int, mandatory
			e = toint(e, "lastKnownMaster", True)
			# repeaterid as int, mandatory
			e = toint(e, "repeaterid", True)
			# rx as float, mandatory
			e = tofloat(e, "rx", True)
			# tx as float, mandatory
			e = tofloat(e, "tx", True)
			# status as int, mandatory
			e = toint(e, "status", True)
			
			if e["parsing_error"]:
				parsing_errors += 1
			
			out.append(e)
		return (out, parsing_errors)
	
	# fetch data
	return fetch('repeater/?action=LIST', callback=cb, cachefile=cachedir+'/repeater.json', cachemaxage=repeater_cache_time_sec)

def repeater_by_radius(rlist, lat, lon, km_radius):
	xoord_range = geobounds(lat, lon, km_radius)
	
	rlist_filtered = []
	for e in rlist:
		if e["lat"] == None:
			continue
		
		if e["lat"] > xoord_range[0][0] and e["lat"] < xoord_range[1][0] and \
		   e["lng"] > xoord_range[0][1] and e["lng"] < xoord_range[1][1]:
			rlist_filtered.append(e)
	
	return rlist_filtered
	
if __name__ == '__main__':
	""" Test code here """
	import pprint
	
	km_file = "DMR Regionen.kml"
	regions = kml.extract_coords(km_file)
	
	sys.stdout.write("Fetching repeater list ...\n")
	rlist, parsing_errors = get_repeater_list()
	sys.stdout.write("	Done. {:d} parsing errors in {} records.\n".format(parsing_errors, len(rlist)))
	#pprint.pprint(rlist)
	
	# find all repeaters within a radius of 200km of the center of switzerland
	# 46.8182° N, 8.2275° E
	xoord_range = geobounds(46.8182, 8.2275, 150)
	rlist_filtered = repeater_by_radius(rlist, 46.8182, 8.2275, 200)
	
	# pprint.pprint(rlist_filtered)
	
	# start csv logger
	f = open('Repeater.csv', 'w', newline='')
	writer = csv.writer(f, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)
	writer.writerow(["callsign", "repeater_id", "colorcode", "lat", "lng", "agl", "rx", "tx", "tg", "ts", "last_updated", "city", "firmware", "hardware", "status", "website", "zone"])
	
	# get detail information for all routers
	rlist_details = []
	for r in rlist_filtered:
		
		# skip MMDV hardware
		if r["hardware"][0:5] == "MMDVM":
			continue
		
		# skip repeaters without shift
		if r["rx"] == r["tx"]: # skip simplex systems
			continue

		# debug code
		#if r["callsign"][0:3] != "HB9": # drill down swiss reptr only, for testing purposes
		#	continue
		
		# fetch talk group info for repeater
		# https://api.brandmeister.network/v1.0/repeater/?action=get&q=204342
		details = fetch("repeater/?action=profile&q="+str(r["repeaterid"]),
		                cachefile='cache/repeatertg/'+str(r["repeaterid"])+'.json', cachemaxage=60*60)
		
		# find zone
		zone = []
		zonenum = 0
		for rname, rpoly in regions:
			inside = point_inside_polygon(r["lng"], r["lat"], rpoly)
			if inside: 
				zone.append(zonenum)
			zonenum += 1
		
		if zone == []:
			continue
		
		for tg in details["staticSubscriptions"]:
			print("		", r["callsign"], r["repeaterid"], tg["talkgroup"], tg["slot"])
			writer.writerow([r["callsign"], r["repeaterid"], r["colorcode"], 
				r["lat"], r["lng"], r["agl"], r["rx"], r["tx"], tg["talkgroup"], 
				tg["slot"], r["last_updated"].strftime("%Y-%m-%d %H:%M:%S"), 
				r["city"], r["firmware"], r["hardware"], r["status"], r["website"], ','.join([str(i) for i in zone])])
