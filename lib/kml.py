#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
This is the communication library for the Brandmeister.Network WebService API
(hence the name)
"""
import os, sys, math
sys.path.append(os.path.abspath('./lib'))
import xml.etree.ElementTree as ET

def parse(km_file):
	""" get an xml parser root tree """
	tree = ET.parse(km_file)
	return tree.getroot()

def extract_coords(km_file):
	""" extract all polygon lines from a google earth kml file """
	
	root = parse(km_file)
	
	ns = "{http://www.opengis.net/kml/2.2}"
	coords = []
	
	res = root.findall(".//{0}Placemark".format(ns))
	for el in root.iter("{0}Placemark".format(ns)):
		#print(el)
		name = None
		c = None
		for eln in el.iter("{0}name".format(ns)): name = eln.text.strip()
		for elc in el.iter("{0}coordinates".format(ns)): c = elc.text.strip()
		
		if c != None:
			polygon = []
			crds = c.split(" ")
			for p in crds:
				lat, lng, alt = p.split(",")
				polygon.append((float(lat), float(lng)))
			del polygon[-1]
			coords.append((name, polygon))
	
	return coords
	
	
if __name__ == '__main__':
	""" Test code here """
	km_file = sys.argv[1]
	c = extract_coords(km_file)
	#print(c)
	
	from shapely.geometry import Polygon, Point
	basel = Point(7.5886, 47.5596)
	#print(c[0][1])
	for r in c:
		poly = Polygon(r[1])
		print(r[0], poly.contains(basel))
	
	
	print(poly)
	
	"""
	for child in root:
		print(child.tag, child.attrib)
	"""