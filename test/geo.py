#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os, sys, math
sys.path.append(os.path.abspath('./lib'))
import geopy
import geopy.distance

hb9dm = (47.416672, 7.75)

distance = 10 # km
diagonal = math.sqrt(distance**2*2)
d = geopy.distance.distance(kilometers = diagonal)

# point top left
latitude, longitude, altitude = d.destination(point=hb9dm, bearing=315)
print("Top left:     {}, {}".format(latitude, longitude))

#point bottom right
latitude, longitude, altitude = d.destination(point=hb9dm, bearing=135)
print("Bottom right: {}, {}".format(latitude, longitude))

def geobounds(lat, lon, km):
	diagonal = math.sqrt(km**2*2)
	d = geopy.distance.distance(kilometers = diagonal)
	
	lat1, lon1, alt2 = d.destination(point=(lat, lon), bearing=315)
	lat2, lon2, alt2 = d.destination(point=(lat, lon), bearing=135)
	
	return [
		(lat1, lon1),
		(lat2, lon2)
	]

print(geobounds(47.416672, 7.75, 10))