#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from urllib import request
import math
import sys

url="https://www.radioid.net/static/users.json"
filename="contacts.json"

with request.urlopen(url) as r:
	"""
	print(r.getcode())  # http response code
	print(r.info())     # all header info 
	"""
	length = int(r.headers["content-length"])
	blocksize = int(math.ceil(length/100.))
	
	f = open("contacts.json", "wb")
	for progress in iter(range(100)):
		fill = "#" * int(progress / 2)
		empty = "." * int((100 - progress) / 2)
		sys.stdout.write("\r{:3d}% {}{}".format(progress, fill, empty))
		sys.stdout.flush()
		f.write(r.read(blocksize))
	sys.stdout.write("\n")
	
	"""
    r = manager.urlopen('GET', url, preload_content=False)
    filesize = r.getheaders()['content-length']
    blocksize = int(math.ceil(filesize/100.))
    for progress in xrange(100):
        print("{d}", progress)
        f.write(r.read(blocksize))
	"""